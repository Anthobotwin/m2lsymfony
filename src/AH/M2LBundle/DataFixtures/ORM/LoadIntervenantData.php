<?php
namespace AH\M2LBundle\DataFixtures\ORM;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AH\M2LBundle\Entity\Intervenant;

class LoadIntervenantData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    { // creation de l'intervenant Albert LINGO, pas de n° à gérer car c'est autoincrémenté
        $unIntervenant = new Intervenant();
        $unIntervenant->setNom('AIRE');
        $unIntervenant->setPrenom('Axel');
        $manager->persist($unIntervenant);
        $this->addReference('AxelAIRE', $unIntervenant);
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}