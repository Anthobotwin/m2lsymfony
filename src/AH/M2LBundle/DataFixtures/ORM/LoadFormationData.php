<?php
namespace AH\M2LBundle\DataFixtures\ORM;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AH\M2LBundle\Entity\Formation;

class LoadFormationData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    { // creation de l'intervenant Albert LINGO, pas de n° à gérer car c'est autoincrémenté
        $uneFormation = new Formation();
        $uneFormation->setDate(new \DateTime('2017-12-28'));
        $uneFormation->setHoraire('8h a 16h');
        $uneFormation->setCout('60 euros');
        $uneFormation->setRepas('non inclus');
        $uneFormation->setDatelimite(new \DateTime('2017-12-25'));
        $uneFormation->setLaFormationSport($manager->merge($this->getReference('formasport')));
        $uneFormation->setLaSalle($manager->merge($this->getReference('laSalle')));
        $uneFormation->setNbinscrit(0);
        $manager->persist($uneFormation);
        $this->addReference('formation', $uneFormation);
        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}