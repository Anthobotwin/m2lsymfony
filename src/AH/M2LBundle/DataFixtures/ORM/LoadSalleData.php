<?php
namespace AH\M2LBundle\DataFixtures\ORM;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AH\M2LBundle\Entity\Salle;

class LoadSalleData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    { // creation de l'intervenant Albert LINGO, pas de n° à gérer car c'est autoincrémenté
        $uneSalle = new Salle();
        $uneSalle->setNomsalle('S12');
        $uneSalle->setAdressesalle('14 rue du marathon 62400 BETHUNE');
        $manager->persist($uneSalle);
        $this->addReference('laSalle', $uneSalle);
        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}