<?php
namespace AH\M2LBundle\DataFixtures\ORM;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AH\M2LBundle\Entity\Formationsport;

class LoadFormationSportData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    { // creation de l'intervenant Albert LINGO, pas de n° à gérer car c'est autoincrémenté
        $uneFormationSport = new Formationsport();
        $uneFormationSport->setTitre('L\'entrainement');
        $uneFormationSport->setObjectif('Être au top le jour J');
        $uneFormationSport->setLogo('entrainement.jpg');
        $uneFormationSport->setNbplace(10);
        $manager->persist($uneFormationSport);
        $this->addReference('formasport', $uneFormationSport);
        $manager->flush();

    }

    public function getOrder()
    {
        return 2;
    }
}