<?php
namespace AH\M2LBundle\DataFixtures\ORM;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AH\M2LBundle\Entity\Intervention;

class LoadInterventionData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    { // creation de l'intervenant Albert LINGO, pas de n° à gérer car c'est autoincrémenté
        $uneIntervention = new Intervention();
        $uneIntervention->setLaFormation($manager->merge($this->getReference('formation')));
        $uneIntervention->setLeIntervenant($manager->merge($this->getReference('AxelAIRE')));
        $manager->persist($uneIntervention);
        $this->addReference('intervention', $uneIntervention);
        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}