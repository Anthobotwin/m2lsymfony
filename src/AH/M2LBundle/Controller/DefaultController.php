<?php

namespace AH\M2LBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AHM2LBundle:Default:index.html.twig');
    }

    public function salleAction(){
        $repo = $this->getDoctrine()->getManager()->getRepository('AHM2LBundle:Salle');
        $listeSalle= $repo->findAll();

        return $this->render('AHM2LBundle:Default:listedessalles.html.twig', array('laListe' => $listeSalle));
    }

    public function intervenantAction(){
        $repo = $this->getDoctrine()->getManager()->getRepository('AHM2LBundle:Intervenant');
        $listeIntervenant= $repo->findAll();

        return $this->render('AHM2LBundle:Default:listedesintervenants.html.twig', array('laListe' => $listeIntervenant));
    }

}
