<?php

namespace AH\M2LBundle\Controller;

use AH\M2LBundle\Entity\Contenu;
use AH\M2LBundle\Entity\ContenuDetail;
use AH\M2LBundle\Entity\Formationinformatique;
use AH\M2LBundle\Entity\Formationsport;
use AH\M2LBundle\Entity\Intervention;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FormationController extends Controller
{
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('AHM2LBundle:Formation');
        $listeFormation = $repo->findAll();

        return $this->render('AHM2LBundle:Formation:listedesformations.html.twig', array('laListe' => $listeFormation));
    }

    public function voirFormationAction($id){
        $repo = $this->getDoctrine()->getManager()->getRepository('AHM2LBundle:Formation');
        $laFormation = $repo->findOneByidf($id);

        if(is_null($laFormation->getLaFormationInfo())) // si la formation est sportive
        {
            return $this->render('AHM2LBundle:Formation:voirformationSportive.html.twig', array('laFormation' => $laFormation));
        }else{ // sinon elle est informatique
            $repoContenu = $this->getDoctrine()->getManager()->getRepository(Contenu::class);
            $leContenu = $repoContenu->findBylaFormationInfo($laFormation->getLaFormationInfo());
            $repoContenuDetail = $this->getDoctrine()->getManager()->getRepository(ContenuDetail::class);

            return $this->render('AHM2LBundle:Formation:voirformationInformatique.html.twig', array('laFormation' => $laFormation,'leContenu' => $leContenu));
        }
    }

    public function ajoutInformatiqueAction(Request $request)
    {
        //mise en place du formulaire
        $uneFormationInfo = new Formationinformatique();
        $leForm = $this->createForm(\AH\M2LBundle\Form\FormationinformatiqueType::class, $uneFormationInfo);
        //la construction du formulaire côté contrôleur s'effectue grâce à la méthode create() du service form . factory .
        // Ensuite on passe la méthode createView() du formulaire à la vue afin qu'elle puisse afficher le formulaire toute seule

        // c’est une autre possibilté pour hydrater l’objet
        if ($request->isMethod('POST')) {
            $leForm->handleRequest($request);
            //si les contraintes sont respectées, enregistrement de la nouvelle salle
            if ($leForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($uneFormationInfo);
                $em->flush();
                //facultatif : message à afficher en cas de succès:
                $request->getSession()->getFlashBag()->add('info', 'formation bien enregistrée.');
                // On redirige vers la page de visualisation de l'annonce nouvellement créée
                return $this->redirectToRoute('ah_m2l_listeformation', array('id' => $uneFormationInfo->getIdfi()));
            }
        }

        return $this->render('AHM2LBundle:Formation:ajoutInformatique.html.twig', array('form' => $leForm->createView()));
    }
}
