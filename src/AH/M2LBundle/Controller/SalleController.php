<?php

namespace AH\M2LBundle\Controller;

use AH\M2LBundle\Entity\Salle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SalleController extends Controller
{
    public function indexAction()
    {
        $listeSalle= $this->getDoctrine()
            ->getEntityManager()
            ->getRepository('AHM2LBundle:Salle')
            ->listeSalle();
        $listeSalleAvecFormat = $this->getDoctrine()
            ->getEntityManager()
            ->getRepository('AHM2LBundle:Salle')
            ->listeSalleAvecNbForma();

        return $this->render('AHM2LBundle:Salle:listedessalles.html.twig', array('laListe' => $listeSalleAvecFormat));
    }

    public function voirAction($id){
        $repo = $this->getDoctrine()->getManager()->getRepository('AHM2LBundle:Salle');
        $laSalle = $repo->findOneByids($id);

        return $this->render('AHM2LBundle:Salle:voir.html.twig', array('laSalle' => $laSalle));
    }

    public function ajoutAction(Request $request)
    {
        //mise en place du formulaire
        $uneSalle = new Salle();
        $leForm = $this->createForm(\AH\M2LBundle\Form\SalleType::class, $uneSalle);
        //la construction du formulaire côté contrôleur s'effectue grâce à la méthode create() du service form . factory .
        // Ensuite on passe la méthode createView() du formulaire à la vue afin qu'elle puisse afficher le formulaire toute seule

        // c’est une autre possibilté pour hydrater l’objet
        if ($request->isMethod('POST')) {
            $leForm->handleRequest($request);
            //si les contraintes sont respectées, enregistrement de la nouvelle salle
            if ($leForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($uneSalle);
                $em->flush();
                //facultatif : message à afficher en cas de succès:
                $request->getSession()->getFlashBag()->add('info', 'Salle bien enregistrée.');
                // On redirige vers la page de visualisation de l'annonce nouvellement créée
                return $this->redirectToRoute('ah_m2l_voirsalle', array('id' => $uneSalle->getIds()));
            }
        }

        return $this->render('AHM2LBundle:Salle:ajout.html.twig', array('form' => $leForm->createView()));
    }
}
