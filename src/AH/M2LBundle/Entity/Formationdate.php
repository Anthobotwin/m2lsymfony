<?php

namespace AH\M2LBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formationdate
 *
 * @ORM\Table(name="formationdate", indexes={@ORM\Index(name="idF", columns={"idF"})})
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\FormationdateRepository")
 */

class Formationdate
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="date", nullable=false)
     * @ORM\Id
     */
    private $date;

    /**
     * @var \Formation
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Formation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idF", referencedColumnName="idF", nullable=false)
     * })
     */
    private $laFormation;



    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Formationdate
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set idf
     *
     * @param \AH\M2LBundle\Entity\Formation $laFormation
     * @return Formationdate
     */
    public function setLaFormation(\AH\M2LBundle\Entity\Formation $laFormation)
    {
        $this->laFormation = $laFormation;

        return $this;
    }

    /**
     * Get idf
     *
     * @return \AH\M2LBundle\Entity\Formation 
     */
    public function getLaFormation()
    {
        return $this->laFormation;
    }
}
