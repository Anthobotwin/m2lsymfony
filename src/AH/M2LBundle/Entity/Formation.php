<?php

namespace AH\M2LBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Formation
 *
 * @ORM\Table(name="formation", indexes={@ORM\Index(name="IDX_C2B1A31CDA21846A", columns={"idFSport"}), @ORM\Index(name="IDX_C2B1A31C20EF0096", columns={"idFInfo"}), @ORM\Index(name="idSalle", columns={"idSalle"})})
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idF", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="Horaire", type="string", length=30, nullable=false)
     */
    private $horaire;

    /**
     * @var string
     *
     * @ORM\Column(name="Cout", type="string", length=100, nullable=false)
     */
    private $cout;

    /**
     * @var string
     *
     * @ORM\Column(name="Repas", type="string", length=100, nullable=false)
     */
    private $repas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateLimite", type="date", nullable=false)
     */
    private $datelimite;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbInscrit", type="integer", nullable=false)
     */
    private $nbinscrit;

    /**
     * @var \Salle
     *
     * @ORM\ManyToOne(targetEntity="Salle",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSalle", referencedColumnName="idS", nullable=false)
     * })
     */
    private $laSalle;

    /**
     * @var \Formationinformatique
     *
     * @ORM\ManyToOne(targetEntity="Formationinformatique",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFInfo", referencedColumnName="idFI", nullable=true)
     * })
     */
    private $laFormationInfo;

    /**
     * @var \Formationsport
     *
     * @ORM\ManyToOne(targetEntity="Formationsport",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFSport", referencedColumnName="idFS", nullable=true)
     * })
     */
    private $laFormationSport;

    /**
     * @ORM\OneToMany(targetEntity="Intervention", mappedBy="laFormation")
     */
    private $lesInterventions;

    public function __construct()
    {
        $this->lesInterventions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getLesInterventions()
    {
        return $this->lesInterventions;
    }

    /**
     * @param mixed $lesInterventions
     */
    public function setLesInterventions($lesInterventions)
    {
        $this->lesInterventions = $lesInterventions;
    }



    /**
     * Get idf
     *
     * @return integer 
     */
    public function getIdf()
    {
        return $this->idf;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Formation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set horaire
     *
     * @param string $horaire
     * @return Formation
     */
    public function setHoraire($horaire)
    {
        $this->horaire = $horaire;

        return $this;
    }

    /**
     * Get horaire
     *
     * @return string 
     */
    public function getHoraire()
    {
        return $this->horaire;
    }

    /**
     * Set cout
     *
     * @param string $cout
     * @return Formation
     */
    public function setCout($cout)
    {
        $this->cout = $cout;

        return $this;
    }

    /**
     * Get cout
     *
     * @return string 
     */
    public function getCout()
    {
        return $this->cout;
    }

    /**
     * Set repas
     *
     * @param string $repas
     * @return Formation
     */
    public function setRepas($repas)
    {
        $this->repas = $repas;

        return $this;
    }

    /**
     * Get repas
     *
     * @return string 
     */
    public function getRepas()
    {
        return $this->repas;
    }

    /**
     * Set datelimite
     *
     * @param \DateTime $datelimite
     * @return Formation
     */
    public function setDatelimite($datelimite)
    {
        $this->datelimite = $datelimite;

        return $this;
    }

    /**
     * Get datelimite
     *
     * @return \DateTime 
     */
    public function getDatelimite()
    {
        return $this->datelimite;
    }

    /**
     * Set nbinscrit
     *
     * @param integer $nbinscrit
     * @return Formation
     */
    public function setNbinscrit($nbinscrit)
    {
        $this->nbinscrit = $nbinscrit;

        return $this;
    }

    /**
     * Get nbinscrit
     *
     * @return integer 
     */
    public function getNbinscrit()
    {
        return $this->nbinscrit;
    }

    /**
     * Set idsalle
     *
     * @param \AH\M2LBundle\Entity\Salle $laSalle
     * @return Formation
     */
    public function setLaSalle(\AH\M2LBundle\Entity\Salle $laSalle = null)
    {
        $this->laSalle = $laSalle;

        return $this;
    }

    /**
     * Get idsalle
     *
     * @return \AH\M2LBundle\Entity\Salle 
     */
    public function getLaSalle()
    {
        return $this->laSalle;
    }

    /**
     * Set idfinfo
     *
     * @param \AH\M2LBundle\Entity\Formationinformatique $laFormationInfo
     * @return Formation
     */
    public function setLaFormationInfo(\AH\M2LBundle\Entity\Formationinformatique $laFormationInfo = null)
    {
        $this->laFormationInfo = $laFormationInfo;

        return $this;
    }

    /**
     * Get idfinfo
     *
     * @return \AH\M2LBundle\Entity\Formationinformatique 
     */
    public function getLaFormationInfo()
    {
        return $this->laFormationInfo;
    }

    /**
     * Set idfsport
     *
     * @param \AH\M2LBundle\Entity\Formationsport $laFormationSport
     * @return Formation
     */
    public function setLaFormationSport(\AH\M2LBundle\Entity\Formationsport $laFormationSport = null)
    {
        $this->laFormationSport = $laFormationSport;

        return $this;
    }

    /**
     * Get idfsport
     *
     * @return \AH\M2LBundle\Entity\Formationsport 
     */
    public function getLaFormationSport()
    {
        return $this->laFormationSport;
    }
}
