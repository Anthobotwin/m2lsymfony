<?php

namespace AH\M2LBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Intervention
 *
 * @ORM\Table(name="intervention", indexes={@ORM\Index(name="IDX_2C2D34875200282E", columns={"idFormation"}), @ORM\Index(name="IDX_2C2D3487AB9A1716", columns={"idIntervenant"})})
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\InterventionRepository")
 */
class Intervention
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idIntervention", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idintervention;

    /**
     * @var \Formation
     *
     * @ORM\ManyToOne(targetEntity="Formation",inversedBy="lesInterventions",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFormation", referencedColumnName="idF", nullable=false)
     * })
     */
    private $laFormation;

    /**
     * @var \Intervenant
     *
     * @ORM\ManyToOne(targetEntity="Intervenant",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idIntervenant", referencedColumnName="idI", nullable=false)
     * })
     */
    private $leIntervenant;



    /**
     * Get idintervention
     *
     * @return integer 
     */
    public function getIdintervention()
    {
        return $this->idintervention;
    }

    /**
     * Set idformation
     *
     * @param \AH\M2LBundle\Entity\Formation $laFormation
     * @return Intervention
     */
    public function setLaFormation(Formation $laFormation = null)
    {
        $this->laFormation = $laFormation;

        return $this;
    }

    /**
     * Get idformation
     *
     * @return \AH\M2LBundle\Entity\Formation 
     */
    public function getLaFormation()
    {
        return $this->laFormation;
    }

    /**
     * Set idintervenant
     *
     * @param \AH\M2LBundle\Entity\Intervenant $leIntervenant
     * @return Intervention
     */
    public function setLeIntervenant(\AH\M2LBundle\Entity\Intervenant $leIntervenant = null)
    {
        $this->leIntervenant = $leIntervenant;

        return $this;
    }

    /**
     * Get idintervenant
     *
     * @return \AH\M2LBundle\Entity\Intervenant 
     */
    public function getLeIntervenant()
    {
        return $this->leIntervenant;
    }
}
