<?php

namespace AH\M2LBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formationsport
 *
 * @ORM\Table(name="formationsport")
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\FormationsportRepository")
 */
class Formationsport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idFS", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idfs;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=100, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="Objectif", type="string", length=100, nullable=false)
     */
    private $objectif;

    /**
     * @var string
     *
     * @ORM\Column(name="Logo", type="string", length=100, nullable=false)
     */
    private $logo;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbPlace", type="integer", nullable=false)
     */
    private $nbplace;



    /**
     * Get idfs
     *
     * @return integer 
     */
    public function getIdfs()
    {
        return $this->idfs;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Formationsport
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set objectif
     *
     * @param string $objectif
     * @return Formationsport
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return string 
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Formationsport
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set nbplace
     *
     * @param integer $nbplace
     * @return Formationsport
     */
    public function setNbplace($nbplace)
    {
        $this->nbplace = $nbplace;

        return $this;
    }

    /**
     * Get nbplace
     *
     * @return integer 
     */
    public function getNbplace()
    {
        return $this->nbplace;
    }
}
