<?php

namespace AH\M2LBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Detail
 *
 * @ORM\Table(name="detail")
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\DetailRepository") */
class Detail
{
    /**
     * @var string
     *
     * @ORM\Column(name="idD", type="string", length=4, nullable=false)
     * @ORM\Id
     */
    private $idd;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleD", type="string", length=150, nullable=false)
     */
    private $libelled;



    /**
     * Get idd
     *
     * @return string 
     */
    public function getIdd()
    {
        return $this->idd;
    }

    /**
     * Set libelled
     *
     * @param string $libelled
     * @return Detail
     */
    public function setLibelled($libelled)
    {
        $this->libelled = $libelled;

        return $this;
    }

    /**
     * Get libelled
     *
     * @return string 
     */
    public function getLibelled()
    {
        return $this->libelled;
    }
}
