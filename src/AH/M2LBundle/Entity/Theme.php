<?php

namespace AH\M2LBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Theme
 *
 * @ORM\Table(name="Theme")
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\ThemeRepository")
 */
class Theme
{
    /**
     * @var string
     *
     * @ORM\Column(name="idT", type="string", length=4, nullable=false)
     * @ORM\Id
     */
    private $idt;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleT", type="string", length=100, nullable=false)
     */
    private $libellet;



    /**
     * Get idt
     *
     * @return string 
     */
    public function getIdt()
    {
        return $this->idt;
    }

    /**
     * Set libellet
     *
     * @param string $libellet
     * @return Theme
     */
    public function setLibellet($libellet)
    {
        $this->libellet = $libellet;

        return $this;
    }

    /**
     * Get libellet
     *
     * @return string 
     */
    public function getLibellet()
    {
        return $this->libellet;
    }
}
