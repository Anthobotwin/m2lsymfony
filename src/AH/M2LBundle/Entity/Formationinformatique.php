<?php

namespace AH\M2LBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formationinformatique
 *
 * @ORM\Table(name="formationinformatique")
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\FormationinformatiqueRepository")
 */
class Formationinformatique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idFI", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idfi;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=100, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="Objectif", type="text", length=65535, nullable=false)
     */
    private $objectif;

    /**
     * @var string
     *
     * @ORM\Column(name="Public", type="string", length=150, nullable=false)
     */
    private $public;

    /**
     * @var string
     *
     * @ORM\Column(name="Prerequis", type="text", nullable=false)
     */
    private $prerequis;

    /**
     * @var string
     *
     * @ORM\Column(name="Logo", type="string", length=50, nullable=false)
     */
    private $logo;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbPlace", type="integer", nullable=false)
     */
    private $nbplace;



    /**
     * Get idfi
     *
     * @return integer 
     */
    public function getIdfi()
    {
        return $this->idfi;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Formationinformatique
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set objectif
     *
     * @param string $objectif
     * @return Formationinformatique
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return string 
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Set public
     *
     * @param string $public
     * @return Formationinformatique
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return string 
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set prerequis
     *
     * @param string $prerequis
     * @return Formationinformatique
     */
    public function setPrerequis($prerequis)
    {
        $this->prerequis = $prerequis;

        return $this;
    }

    /**
     * Get prerequis
     *
     * @return string 
     */
    public function getPrerequis()
    {
        return $this->prerequis;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Formationinformatique
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set nbplace
     *
     * @param integer $nbplace
     * @return Formationinformatique
     */
    public function setNbplace($nbplace)
    {
        $this->nbplace = $nbplace;

        return $this;
    }

    /**
     * Get nbplace
     *
     * @return integer 
     */
    public function getNbplace()
    {
        return $this->nbplace;
    }
}
