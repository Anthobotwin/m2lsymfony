<?php

namespace AH\M2LBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Salle
 *
 * @ORM\Table(name="salle")
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\SalleRepository")
 */
class Salle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idS", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $ids;

    /**
     * @var string
     *
     * @ORM\Column(name="nomSalle", type="string", length=50, nullable=false)
     */
    private $nomsalle;

    /**
     * @var string
     *
     * @ORM\Column(name="adresseSalle", type="string", length=100, nullable=false)
     */
    private $adressesalle;



    /**
     * Get ids
     *
     * @return integer 
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * Set nomsalle
     *
     * @param string $nomsalle
     * @return Salle
     */
    public function setNomsalle($nomsalle)
    {
        $this->nomsalle = $nomsalle;

        return $this;
    }

    /**
     * Get nomsalle
     *
     * @return string 
     */
    public function getNomsalle()
    {
        return $this->nomsalle;
    }

    /**
     * Set adressesalle
     *
     * @param string $adressesalle
     * @return Salle
     */
    public function setAdressesalle($adressesalle)
    {
        $this->adressesalle = $adressesalle;

        return $this;
    }

    /**
     * Get adressesalle
     *
     * @return string 
     */
    public function getAdressesalle()
    {
        return $this->adressesalle;
    }
}
