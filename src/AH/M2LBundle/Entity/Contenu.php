<?php

namespace AH\M2LBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Contenu
 *
 * @ORM\Table(name="contenu", indexes={@ORM\Index(name="idForInfo", columns={"idForInfo", "idTheme"}), @ORM\Index(name="idTheme", columns={"idTheme"}), @ORM\Index(name="idForInfo_2", columns={"idForInfo", "idTheme"}), @ORM\Index(name="IDX_89C2003F55FC8206", columns={"idForInfo"})})
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\ContenuRepository") */
class Contenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idC", type="integer", nullable=false)
     * @ORM\Id
     */
    private $idc;

    /**
     * @var integer
     *
     * @ORM\Column(name="Ordre", type="integer", nullable=false)
     */
    private $ordre;

    /**
     * @var \Formationinformatique
     *
     * @ORM\ManyToOne(targetEntity="Formationinformatique",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idForInfo", referencedColumnName="idFI", nullable=false)
     * })
     */
    private $laFormationInfo;

    /**
     * @var \Theme
     *
     * @ORM\ManyToOne(targetEntity="Theme",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idTheme", referencedColumnName="idT", nullable=false)
     * })
     */
    private $leTheme;

    /**
     * @ORM\OneToMany(targetEntity="ContenuDetail", mappedBy="leContenu")
     */
    private $lesDetails;

    public function __construct()
    {
        $this->lesDetails = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getLesDetails()
    {
        return $this->lesDetails;
    }

    /**
     * @param mixed $lesDetails
     */
    public function setLesDetails($lesDetails)
    {
        $this->lesDetails = $lesDetails;
    }




    /**
     * Get idc
     *
     * @return integer 
     */
    public function getIdc()
    {
        return $this->idc;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     * @return Contenu
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre
     *
     * @return integer 
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set idforinfo
     *
     * @param \AH\M2LBundle\Entity\Formationinformatique $laFormationInfo
     * @return Contenu
     */
    public function setLaFormationInfo(\AH\M2LBundle\Entity\Formationinformatique $laFormationInfo = null)
    {
        $this->laFormationInfo = $laFormationInfo;

        return $this;
    }

    /**
     * Get idforinfo
     *
     * @return \AH\M2LBundle\Entity\Formationinformatique 
     */
    public function getLaFormationInfo()
    {
        return $this->laFormationInfo;
    }

    /**
     * Set idtheme
     *
     * @param \AH\M2LBundle\Entity\Theme $leTheme
     * @return Contenu
     */
    public function setLeTheme(\AH\M2LBundle\Entity\Theme $leTheme = null)
    {
        $this->leTheme = $leTheme;

        return $this;
    }

    /**
     * Get idtheme
     *
     * @return \AH\M2LBundle\Entity\Theme 
     */
    public function getLeTheme()
    {
        return $this->leTheme;
    }
}
