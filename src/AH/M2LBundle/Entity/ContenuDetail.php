<?php

namespace AH\M2LBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContenuDetail
 *
 * @ORM\Table(name="contenu_detail", indexes={@ORM\Index(name="contdet", columns={"idDetail"}), @ORM\Index(name="idContenu", columns={"idContenu"})})
 * @ORM\Entity(repositoryClass="AH\M2LBundle\Repository\ContenuDetailRepository") */
class ContenuDetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idContenuDetail", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idcontenudetail;

    /**
     * @var \Contenu
     *
     * @ORM\ManyToOne(targetEntity="Contenu",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idContenu", referencedColumnName="idC", nullable=false)
     * })
     */
    private $leContenu;

    /**
     * @var \Detail
     *
     * @ORM\ManyToOne(targetEntity="Detail",cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idDetail", referencedColumnName="idD", nullable=false)
     * })
     */
    private $leDetail;



    /**
     * Get idcontenudetail
     *
     * @return integer 
     */
    public function getIdcontenudetail()
    {
        return $this->idcontenudetail;
    }

    /**
     * Set idcontenu
     *
     * @param \AH\M2LBundle\Entity\Contenu $leContenu
     * @return ContenuDetail
     */
    public function setLeContenu(\AH\M2LBundle\Entity\Contenu $leContenu = null)
    {
        $this->leContenu = $leContenu;

        return $this;
    }

    /**
     * Get idcontenu
     *
     * @return \AH\M2LBundle\Entity\Contenu 
     */
    public function getLeContenu()
    {
        return $this->leContenu;
    }

    /**
     * Set iddetail
     *
     * @param \AH\M2LBundle\Entity\Detail $leDetail
     * @return ContenuDetail
     */
    public function setLeDetail(\AH\M2LBundle\Entity\Detail $leDetail = null)
    {
        $this->leDetail = $leDetail;

        return $this;
    }

    /**
     * Get iddetail
     *
     * @return \AH\M2LBundle\Entity\Detail 
     */
    public function getLeDetail()
    {
        return $this->leDetail;
    }
}
