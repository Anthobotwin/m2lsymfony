<?php

namespace AH\M2LBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ContenuDetailRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ContenuDetailRepository extends EntityRepository
{
}
